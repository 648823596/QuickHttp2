package cn.schoolwow.quickhttp.domain;

/**日志等级*/
public enum LogLevel {
    TRACE,
    DEBUG,
    INFO,
    WARN,
    ERROR;
}
