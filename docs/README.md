# QuickHttp2

简单易用的Java Http客户端.

[中文文档](/zh-cn/)/[English](/en/)

# 反馈

若有问题请提交Issue或者发送邮件到648823596@qq.com.

# 开源协议
本软件使用[LGPL](http://www.gnu.org/licenses/lgpl-3.0-standalone.html)开源协议!